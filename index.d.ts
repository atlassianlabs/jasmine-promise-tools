export function failTest(fn: () => void): (err: any) => void;
export function willResolve(fn: () => Promise<any>): () => void;
export function expectToReject(promise: Promise<any>): Promise<any>;
