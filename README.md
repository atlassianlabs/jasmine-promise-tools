![build-status](https://bitbucket-badges.atlassian.io/badge/atlassianlabs/jasmine-promise-tools.svg)

Testing promises in Jasmine can be tricky and error-prone. This library has a few simple and general purpose functions to make it safer and easier to write async promise-based tests in Jasmine.

# Functionality

All these functions provide detailed and helpful output in the case of a test failure.

* `willResolve` function converts a promise returning function into a Jasmine async function.
* `expectToReject` function fails the test if the promise resolves unexpectedly.
* `failTest` function returns a function which will fail a Jasmine async test.


# Example usage

It's recommended to add a Jasmine helper file so willResolve() and expectToReject() are available as globals for all your tests.

```
const {willResolve, expectToReject} = require('jasmine-promise-tools');

global.willResolve = willResolve;
global.expectToReject = expectToReject;
```


```
it('passes when a promise is resolved', willResolve(() => {
    return doThingAndSucceed()
        .then(() => {
            expect(1).toBe(1);
        });
}));

it('passes when you expect a promise to reject', willResolve(() => {
    const promise = doThingAndFail();

    return expectToReject(promise)
        .then((err) => {
            expect(err).toBe(42);
        });
}));
```


# Development guide

## Install dependencies

```
npm install
```


## Useful commands

```
# Run all checks
npm test

# Run just the jasmine tests
npm run test:jasmine

# Run just the linter
npm run test:lint
```


## Perform a release

```
npm version 99.98.97
npm publish
git push
git push --tags
```


## Contributors

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record stating that the contributor is entitled to contribute the code/documentation/translation to the project and is willing to have it used in distributions and derivative works (or is willing to transfer ownership).

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)


# License

Copyright (c) 2016 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
