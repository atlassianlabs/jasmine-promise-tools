'use strict';

module.exports = {
    message: 'This is a special sentinel object. If willResolve() catches it then it will NOT fail your test'
};
