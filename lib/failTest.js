'use strict';

const generateErrorMessage = require('./generateErrorMessage');

function failTest(done) {
    return function failTestNow(err) {
        let message = 'The promise failed or succeeded when the opposite was expected.';
        message += ' ' + generateErrorMessage(err);
        done.fail(message);
    };
}

module.exports = failTest;
