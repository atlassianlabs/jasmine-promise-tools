'use strict';

const resolvedSentinel = require('./resolvedSentinel');

function expectToReject(promise) {
    return promise
        .then(
            () => {
                fail('Expected promise to reject!');
                throw resolvedSentinel;
            },
            (err) => {
                // resolve this promise so callers can chain with .then() and inspect the value
                return err;
            }
        );
}

module.exports = expectToReject;
