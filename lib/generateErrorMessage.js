'use strict';

function generateErrorMessage(err) {
    if (!err) {
        return 'No error was supplied!';
    }

    let message = 'See detail below:';

    if (err.stack) {
        message += '\nerr.stack: ' + err.stack;
    } else if (err.message) {
        message += '\nerr.message: ' + err.message;
    }

    try {
        message += '\nerr: ' + JSON.stringify(err, null, 2);
    } catch (err2) {
        message += '\nerr: ' + err;
    }

    return message;
}

module.exports = generateErrorMessage;
