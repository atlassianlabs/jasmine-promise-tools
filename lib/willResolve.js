'use strict';

const generateErrorMessage = require('./generateErrorMessage');
const resolvedSentinel = require('./resolvedSentinel');

function ensureIsPromise(p) {
    if (!p || !p.then) {
        throw new Error('You must return a Promise');
    }
}

function handleError(done, err) {
    if (err === resolvedSentinel) {
        done();
        return;
    }

    done.fail('The promise rejected! ' + generateErrorMessage(err));
}

function willResolve(fn) {
    return function waitForResolvedPromise(done) {
        const p = fn();
        ensureIsPromise(p);
        p.then(done, (err) => handleError(done, err));
    };
}

module.exports = willResolve;
