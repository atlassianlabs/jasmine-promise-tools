'use strict';

/*
 * In most cases you should prefer to use jasminePromise over failTest.
 */

const failTest = require('../../lib/failTest');

describe('examples/failTest', () => {

    it('normal configuration passes when a promise is resolved', (done) => {
        Promise.resolve()
            .then(() => {
                expect(1).toBe(1);
                done();
            })
            .catch(failTest(done));
    });

    it('opposite configuration passes when a promise is rejected', (done) => {
        Promise.reject()
            .then(failTest(done))
            .catch(() => {
                expect(1).toBe(1);
                done();
            });
    });

});
