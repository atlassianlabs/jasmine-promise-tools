'use strict';

const willResolve = require('../../lib/willResolve');
const expectToReject = require('../../lib/expectToReject');

function doThingAndSucceed() {
    return Promise.resolve();
}

function doThingAndFail() {
    return Promise.reject(42);
}

describe('examples/willResolve', () => {

    it('passes when a promise is resolved', willResolve(() => {
        return doThingAndSucceed()
            .then(() => {
                expect(1).toBe(1);
            });
    }));

    it('passes when you expect a promise to reject', willResolve(() => {
        const promise = doThingAndFail();

        return expectToReject(promise)
            .then((err) => {
                expect(err).toBe(42);
            });
    }));

    it('passes when you convert a reject into a resolve', willResolve(() => {
        // you may prefer the style above, it's a little clearer

        return doThingAndFail()
            .then(() => {
                fail('Expected promise to reject!');
            }, (err) => {
                expect(err).toBe(42);
            });
    }));

});
