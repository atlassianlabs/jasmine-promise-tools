'use strict';

const willResolve = require('../../lib/willResolve');

describe('unit/willResolve', () => {

    let mockDone;

    beforeEach(() => {
        mockDone = jasmine.createSpy('done');
        mockDone.fail = jasmine.createSpy('done.fail');
    });

    it('throws if nothing is returned', () => {
        try {
            willResolve(() => {
                return;
            })(mockDone);
            fail('expected an error to be thrown');
        } catch (err) {
            expect(err.message).toBe('You must return a Promise');
        }
    });

    it('throws if a non-promise is returned', () => {
        try {
            willResolve(() => {
                return {'not-a-promise': true};
            })(mockDone);
            fail('expected an error to be thrown');
        } catch (err) {
            expect(err.message).toBe('You must return a Promise');
        }
    });

    it('calls the function it is passed', () => {
        const fn = jasmine.createSpy('fn').and.returnValue(Promise.resolve());

        willResolve(fn)(mockDone);

        expect(fn.calls.count()).toBe(1);
    });

    it('calls done() if the promise resolves', (done) => {
        willResolve(() => {
            return Promise.resolve();
        })(mockDone);

        // a little dodgy...
        setImmediate(() => {
            expect(mockDone.calls.count()).toBe(1);
            done();
        });
    });

    it('calls done.fail() if the promise rejects', (done) => {
        willResolve(() => {
            return Promise.reject('some error');
        })(mockDone);

        // a little dodgy...
        setImmediate(() => {
            expect(mockDone.fail.calls.count()).toBe(1);
            done();
        });
    });

    it('generates a useful error message if the promise rejects', (done) => {
        willResolve(() => {
            return Promise.reject('some error');
        })(mockDone);

        // a little dodgy...
        setImmediate(() => {
            const failMessage = mockDone.fail.calls.argsFor(0)[0];
            expect(failMessage).toMatch(/The promise rejected! See detail below/);
            expect(failMessage).toMatch(/some error/);
            done();
        });
    });

});
