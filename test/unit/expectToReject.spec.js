'use strict';

const willResolve = require('../../lib/willResolve');
const expectToReject = require('../../lib/expectToReject');

describe('unit/expectToReject', () => {

    let mockFail;
    let jasmineFail;

    beforeEach(() => {
        jasmineFail = global.fail;

        mockFail = jasmine.createSpy('fail');
        global.fail = mockFail;
    });

    afterEach(() => {
        global.fail = jasmineFail;
    });


    it('to fail the test if the promise is resolved', willResolve(() => {
        const promise = Promise.resolve();

        return expectToReject(promise)
            .then(() => {
                expect(mockFail).toHaveBeenCalledWith(jasmine.stringMatching('Expected promise to reject!'));
            });
    }));

    it('throws a special sentinel error if the promise is resolved', willResolve(() => {
        const promise = Promise.resolve();

        return expectToReject(promise)
            .then(() => {
                fail('expectToReject() should reject if the promise resolves!');
            }, (err) => {
                expect(err.message).toMatch(/sentinel/);
            });
    }));

    it('converts a rejected promise into a resolved one', willResolve(() => {
        const promise = Promise.reject('rejected value');

        return expectToReject(promise)
            .then((result) => {
                expect(result).toBe('rejected value');
            });
    }));

});
