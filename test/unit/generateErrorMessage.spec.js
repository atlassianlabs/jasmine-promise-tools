'use strict';

const generateErrorMessage = require('../../index').generateErrorMessage;

describe('unit/generateErrorMessage', () => {

    it('can handle an undefined error', () => {
        const result = generateErrorMessage();

        expect(result).toBe('No error was supplied!');
    });

    it('handles strings as errors', () => {
        const result = generateErrorMessage('dummy-error');

        expect(result).toBe(`See detail below:
err: "dummy-error"`);
    });

    it('handles objects as errors', () => {
        const result = generateErrorMessage({foo: 1, bar: {baz: 2}});

        expect(result).toBe(`See detail below:
err: {
  "foo": 1,
  "bar": {
    "baz": 2
  }
}`);
    });

    it('prints the stack trace if it is present', () => {
        const result = generateErrorMessage({stack: 'the-stack'});

        expect(result).toBe(`See detail below:
err.stack: the-stack
err: {
  "stack": "the-stack"
}`);
    });

    it('prints the message if it is present', () => {
        const result = generateErrorMessage({message: 'the-message'});

        expect(result).toBe(`See detail below:
err.message: the-message
err: {
  "message": "the-message"
}`);
    });

    it('prints an Error correctly', () => {
        const result = generateErrorMessage(new Error('the-message'));

        expect(result).toMatch(/err.stack: Error: the-message/);
        expect(result).not.toMatch(/err.message/);
        expect(result).toMatch(/err: {}/);
    });

    it('handles unstringifiable objects', () => {
        const obj = {};
        obj.obj = obj;
        const result = generateErrorMessage(obj);

        expect(result).toBe(`See detail below:
err: [object Object]`);
    });

});
